pragma circom 2.0.0;

include "../../node_modules/circomlib/circuits/poseidon.circom";

template TreeLevel(leafs, nb_nodes) {
    signal input leaves[leafs];
    signal output nodes[nb_nodes];

     if (leafs > 16) {
         var remainings = leafs%16;
         component hashes[leafs\16 + (remainings != 0)];
         for (var i = 0; i < leafs\16; i++) {
             hashes[i] = Poseidon(16);
             for (var j = 0; j < 16; j++) {
                 hashes[i].inputs[j] <== leaves[i*16+j];
             }
                nodes[i] <== hashes[i].out;
         }
         if (leafs%16 != 0) {
             hashes[leafs\16] = Poseidon(remainings);
             for (var i = 0; i < leafs%16; i++) {
                 hashes[leafs\16].inputs[i] <== leaves[(leafs\16)*16+i];
             }
             nodes[leafs\16] <== hashes[leafs\16].out;
         }
     } else {
         component poseidon = Poseidon(leafs);
         for (var i = 0; i < leafs; i++) {
             poseidon.inputs[i] <== leaves[i];
         }
         nodes[0] <== poseidon.out;
     }
}

template HashTree(nb_leaves) {
    signal input leaves[nb_leaves];
    signal output hash;

    var levels = 0;
    var nodes = 1;
    while (nb_leaves > nodes) {
        levels++;
        nodes *= 16;
    }

    component tree_levels[levels];

    var leafCount = nb_leaves;
    var nodeCount = nb_leaves\16 + (nb_leaves%16 != 0);

    for (var i = 0; i < levels; i++) {
        tree_levels[i] = TreeLevel(leafCount, nodeCount);

        var n = 0;
        while(n < leafCount){
            tree_levels[i].leaves[n] <== i == 0 ? leaves[n] : tree_levels[i - 1].nodes[n];
            n++;
        }
        leafCount = leafCount\16 + (leafCount%16 != 0);
        nodeCount = leafCount\16 + (leafCount%16 != 0);
    }

    hash <== tree_levels[levels - 1].nodes[0];
}

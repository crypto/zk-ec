import {ErasureCode} from "../src/erasure_code";
import assert from "assert";
import {Groth16Verify, signalToBuffer} from "../src/proof";
import {ErasureCodeProver} from "../src/erasure_code.prover";
import {Shard} from "../src/shard";
import {bigintToBuffer, bufferToBigIntArray} from "../src/buffer";

describe("Test EC", function () {

    it("Should serialize and deserialize a shard", async function() {
        const bigBuffer = Buffer.from('0102030405060708090a0b0c0d0e0f', 'hex');
        let ec = await ErasureCode.encode(bigBuffer, 3,4);
        let a = ec[0].data[0];
        let b = await bigintToBuffer(a);
        let d = bufferToBigIntArray(b, 32);
        assert(d[0].toString(),a.toString());
    });

    it("Should decode 15 bytes with a (4,3) ", async function() {
        this.timeout(10000);
        const bigBuffer = Buffer.from('0102030405060708090a0b0c0d0e0f', 'hex');
        let ec = await ErasureCode.encode(bigBuffer, 3,4);

        ec.sort(() => Math.random() - 0.5);
        const picked = ec.reduce((acc: Shard[], s, index) => {
            if (index < 3) {
                acc.push(s);
            }
            return acc;
        }, []);

        let dec = await ErasureCode.decode(picked, 3, 4);
        assert(bigBuffer.equals(dec));
    });

    it("Should decode more bytes with a (4,3) ", async function() {
        const length = 800;
        let bigBuffer = Buffer.alloc(length);
        for (let i = 0; i < length; i++) {
            bigBuffer[i] = Math.floor(Math.random() * 256);
        }
        let ec = await ErasureCode.encode(bigBuffer, 3,4);
        ec.sort(() => Math.random() - 0.5);
        const picked = ec.reduce((acc: Shard[], s, index) => {
            if (index < 3) {
                acc.push(s);
            }
            return acc;
        }, []);
        let dec = await ErasureCode.decode(picked, 3, 4);
        assert(dec.length === bigBuffer.length);
        assert(bigBuffer.equals(dec));
    });

    it("Should prove a shard", async function() {
        const k=3;
        const nbChunks=1;
        let prover = new ErasureCodeProver(k,nbChunks);
        const bigBuffer = Buffer.from('0102030405060708090a0b0c0d0e0f', 'hex');
        let ec = await ErasureCode.encode(bigBuffer, k,k+1);
        for (let i = 0; i < 4; i++) {
            let proof = await prover.proveShard(bigBuffer, i+1);
            console.log(proof);

            let buf = await bigintToBuffer(ec[i].data[0]);
            assert(buf.equals(signalToBuffer(proof.public_signals[0])));

            let vv = await Groth16Verify(`dist/zk/verifiers/erasure_code_${k}_${nbChunks}.vkey.json`, proof);
            assert(vv);
        }
    });

    it("Should prove a shard containing several bigints using k=4", async function() {
        const length = 240;
        const k=4;
        const nbChunks=2;
        let prover = new ErasureCodeProver(k,nbChunks);
        let bigBuffer = Buffer.alloc(length);
        for (let i = 0; i < length; i++) {
            bigBuffer[i] = Math.floor(Math.random() * 256);
        }

        for (let i = 0; i < k+1; i++) {
            let proof = await prover.proveShard(bigBuffer, i+1);

            let vv = await Groth16Verify(`dist/zk/verifiers/erasure_code_${k}_${nbChunks}.vkey.json`, proof);
            assert(vv);
        }
    });

    it("Should prove a shard containing a lot of bigints using k=4", async function() {
        const k=4;
        const nbChunks=8;
        const length = 31 * k * nbChunks - 8;
        let prover = new ErasureCodeProver(k,nbChunks);
        let bigBuffer = Buffer.alloc(length);
        for (let i = 0; i < length; i++) {
            bigBuffer[i] = Math.floor(Math.random() * 256);
        }

        for (let i = 0; i < k+1; i++) {
            let proof = await prover.proveShard(bigBuffer, i+1);

            let vv = await Groth16Verify(`dist/zk/verifiers/erasure_code_${k}_${nbChunks}.vkey.json`, proof);
            assert(vv);
        }
    });
    it("Should prove a shard containing a lot of bigints using k=4 not multiple of 16", async function() {
        const k=4;
        const nbChunks=6;
        const length = 31 * k * nbChunks - 8;
        let prover = new ErasureCodeProver(k,nbChunks);
        let bigBuffer = Buffer.alloc(length);
        for (let i = 0; i < length; i++) {
            bigBuffer[i] = Math.floor(Math.random() * 256);
        }

        for (let i = 0; i < k+1; i++) {
            let proof = await prover.proveShard(bigBuffer, i+1);

            let vv = await Groth16Verify(`dist/zk/verifiers/erasure_code_${k}_${nbChunks}.vkey.json`, proof);
            assert(vv);
        }
    });
});
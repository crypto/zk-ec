import {buildPoseidonOpt} from "circomlibjs";
import assert from "assert";
import Poseidon from "../src/poseidon";

describe("Test poseidon", function () {
    let pos: any;
    beforeEach(async function() {
        pos = await buildPoseidonOpt();
    });
    it("Should get the correct hash", async function() {
        const h = await Poseidon.hash([BigInt(1), BigInt(2)]);
        assert(pos.F.eq(pos.F.e("0x115cc0f5e7d690413df64c6b9662e9cf2a3617f2743245519e19607a4417189a"), pos.F.e(h)));
    });
    it("Should get the correct hash2", async function() {
        const h1 = await Poseidon.hash([BigInt(1), BigInt(2)]);
        const h2 = await Poseidon.hash([BigInt(3), BigInt(4)]);
        const hh = await Poseidon.hash([h1,h2]);
        assert(pos.F.eq(pos.F.e("0x75d30e28d48842bd6c1044b68f982d586e2892ae91c77f8f56111d8f55070ed"), pos.F.e(hh)));
    });
});
import {bigintToBuffer, bufferToBigIntArray} from "../src/buffer";
import assert from "assert";
import {ErasureCode} from "../src/erasure_code";
import LinearCombinationProver from "../src/linear_combination.prover";
import {signalToBuffer} from "../src/proof";
import LinearCombinationVerifier from "../src/linear_combination.verifier";

describe("Test Linear Combination", function () {

    it("Should generate the same linear combination", async function () {
        const k = 3;
        const length = k * 31;
        const bigBuffer = Buffer.alloc(length);
        for (let i = 0; i < length; i++) {
            bigBuffer[i] = Math.floor(Math.random() * 256);
        }        const data = bufferToBigIntArray(bigBuffer, 31);
        assert(data.length === k);

        const shard = await ErasureCode.GenerateLinearCombination(data, k, 2);
        const prover = new LinearCombinationProver(k);
        const proof = await prover.prove(data, BigInt(2));
        const buf = await bigintToBuffer(shard.data[0]);
        assert(buf.equals(signalToBuffer(proof.public_signals[0])));

        const verifier = new LinearCombinationVerifier(k);
        const v = await verifier.verify(proof);
        assert(v);
    });
});
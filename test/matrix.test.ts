import {Matrix} from "../src/matrix";
import assert from "assert";
import ffjs from "ffjavascript";

describe("Test Matrix", function () {
    it("Should create a matrix", async function() {
        const m = await Matrix.createVandermonde(3,3);
        assert.strictEqual(m.rows, 3);
        assert.strictEqual(m.cols, 3);
    });

    it("Should create an identity matrix", async function() {
        let curve = await ffjs.getCurveFromName('bn128',true);
        let F = curve.Fr;
        const m = Matrix.createIdentity(3, F);
        for (let i = 0; i < m.rows; i++) {
            for (let j = 0; j < m.cols; j++) {
                if (i === j) {
                    assert(m.getValue(i, j).toString() === F.e(BigInt(1)).toString());
                } else {
                    assert(m.getValue(i, j).toString() === F.e(BigInt(0)).toString());
                }
            }
        }
    });

    it("Should invert a matrix", async function() {
        const m = await Matrix.createVandermonde(3,3);
        const inv = m.copy().invert();
        const mm = inv.invert();
        for (let i = 0; i < m.rows; i++) {
            for (let j = 0; j < m.cols; j++) {
                assert(m.getValue(i, j).toString() === mm.getValue(i, j).toString());
            }
        }
    });
});
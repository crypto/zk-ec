import {buildPoseidon} from "circomlibjs";

/**
 * Interface for hashable objects.
 */
export interface Hashable {
    /**
     * Method to hash the object.
     * @returns A promise that resolves to a bigint hash of the object.
     */
    hash(): Promise<bigint>;
}

/**
 * Class for Poseidon hash function.
 */
export default class Poseidon {
    /**
     * Static property to hold the Poseidon hash function.
     */
    private static poseidon: any;

    /**
     * Static method to hash a message using the Poseidon hash function.
     * @param message - The message to be hashed, represented as an array of bigints.
     * @returns A promise that resolves to a bigint hash of the message.
     */
    static async hash(message: bigint[]): Promise<bigint> {
        // If the Poseidon hash function has not been built yet, build it.
        Poseidon.poseidon = Poseidon.poseidon || await buildPoseidon();

        // If the message length is less than or equal to 16, hash the entire message at once.
        if (message.length <= 16) {
            return this.poseidon.F.toString(this.poseidon(message));
        }

        // If the message length is greater than 16, divide the message into chunks of 16 and hash each chunk separately.
        const hashes = Array(Math.ceil(message.length / 16)).fill(0).map((_, i) => {
            const chunk = message.slice(i * 16, (i + 1) * 16);
            return this.poseidon(chunk);
        });

        // Recursively hash the array of chunk hashes until a single hash is obtained.
        return Poseidon.hash(hashes);
    }
}
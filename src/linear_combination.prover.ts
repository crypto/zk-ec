import fs from "fs";
import path from "path";
import * as snarkjs from "snarkjs";
import {SnarkProof} from "./proof";

export default class LinearCombinationProver {
    private wasm: Buffer;
    private zKey: Buffer;

    constructor(private k: number) {
        const basePath = path.join(__dirname, `../zk`);
        this.wasm = fs.readFileSync(`${basePath}/circuits/linear_combination_${k}_js/linear_combination_${k}.wasm`);
        this.zKey = fs.readFileSync(`${basePath}/zkeys/linear_combination_${k}_final.zkey`);
    }

    async prove(data: bigint[], alpha: bigint): Promise<SnarkProof> {
        const { proof, publicSignals } = await snarkjs.groth16.fullProve(
            { sources: data, alpha },
            this.wasm,
            this.zKey
        );
        return {
            proof: {
                a: proof.pi_a as [bigint, bigint],
                b: [proof.pi_b[0].reverse(), proof.pi_b[1].reverse()] as [[bigint, bigint], [bigint, bigint]],
                c: proof.pi_c as [bigint, bigint],
            },
            public_signals: publicSignals
        };
    }
}
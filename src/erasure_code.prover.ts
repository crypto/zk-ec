import {SnarkProof} from "./proof";
import * as snarkjs from "snarkjs";
import fs from "fs";
import path from "path";
import Poseidon from "./poseidon";
import {bufferToBigIntArray} from "./buffer";

/**
 * Class representing an Erasure Code Prover.
 */
export class ErasureCodeProver {
    private wasm: Buffer;
    private zKey: Buffer;

    /**
     * Create an ErasureCodeProver.
     * @param {number} _k - The number of data shards.
     * @param {number} nbChunks - The number of chunks in a shard.
     */
    constructor(private _k: number, private nbChunks: number) {
        const basePath = path.join(__dirname, `../zk`);
        console.log('basePath:', basePath);
        this.wasm = fs.readFileSync(`${basePath}/circuits/erasure_code_${_k}_${nbChunks}_js/erasure_code_${_k}_${nbChunks}.wasm`);
        this.zKey = fs.readFileSync(`${basePath}/zkeys/erasure_code_${_k}_${nbChunks}_final.zkey`);
    }

    /**
     * Prove shards.
     * @param {Buffer} data - The data to prove.
     * @param {number} n - The number of shards to generate and prove.
     * @return {Promise<[bigint,SnarkProof[]]>} The hash and proofs.
     */
    async proveShards(data: Buffer, n: number): Promise<[bigint,SnarkProof[]]> {
        data = this.prepareData(data);
        const dataBigInt = bufferToBigIntArray(data,31);
        this.padData(dataBigInt);

        const hash = await Poseidon.hash(dataBigInt);
        const proofs = await Promise.all(Array.from({length: n}, (_, i) => this.prove(dataBigInt, BigInt(i + 1))));

        return [hash, proofs];
    }

    /**
     * Prove shard.
     * @param {BigInt[]} data - The data to prove.
     * @param {bigint} alpha - The alpha value.
     * @return {Promise<SnarkProof>} The proof.
     */
    private async prove(data: bigint[], alpha: bigint): Promise<SnarkProof> {
        const { proof, publicSignals } = await snarkjs.groth16.fullProve(
            { sources: data, alpha },
            this.wasm,
            this.zKey
        );
        return {
            proof:{
                a: proof.pi_a as [bigint, bigint],
                b: [proof.pi_b[0].reverse(), proof.pi_b[1].reverse()] as [[bigint, bigint], [bigint, bigint]],
                c: proof.pi_c as [bigint, bigint],
            },
            public_signals: publicSignals
        };
    }

    /**
     * Prove data.
     * @param {Buffer} data - The data to prove.
     * @param {bigint} alpha - The alpha value.
     * @return {Promise<SnarkProof>} The proof.
     */
    async proveShard(data: Buffer, alpha: number): Promise<SnarkProof> {
        data = this.prepareData(data);
        const dataBigInt = bufferToBigIntArray(data,31);
        this.padData(dataBigInt);

        const hash = await Poseidon.hash(dataBigInt);
        console.log('generated hash in the code:', hash);

        return await this.prove(dataBigInt, BigInt(alpha));
    }

    /**
     * Prepare data.
     * @param {Buffer} data - The data to prepare.
     * @return {Buffer} The prepared data.
     */
    private prepareData(data: Buffer): Buffer {
        const sizeBuffer = Buffer.alloc(8);
        sizeBuffer.writeBigUInt64LE(BigInt(data.length));
        return Buffer.concat([sizeBuffer, data]);
    }

    /**
     * Pad data.
     * @param {BigInt[]} data - The data to pad.
     */
    private padData(data: BigInt[]): void {
        while (data.length % this._k != 0) {
            data.push(BigInt(0));
        }
    }
}
import {Matrix} from "./matrix";
import Poseidon from "./poseidon";
import {Shard} from "./shard";
import {bigIntArrayToBuffer, bufferToBigIntArray} from "./buffer";
import ffjs from "ffjavascript";

/**
 * ErasureCode class provides methods for encoding and decoding data using erasure coding.
 */
export class ErasureCode {
    /**
     * Decodes the given shards back into the original data.
     * @param {Shard[]} shards - The shards to decode.
     * @param {number} k - The number of data shards.
     * @param {number} n - The total number of shards.
     * @returns {Promise<Buffer>} The decoded data.
     */
    static async decode(shards: Shard[], k: number, n: number): Promise<Buffer> {
        // Get the curve
        let curve = await ffjs.getCurveFromName('bn128',true);
        let F = curve.Fr;

        // Create the Vandermonde matrix
        let m = await Matrix.createVandermonde(k,n);
        let col_ids = shards.map(s => s.id);

        let inv = await Matrix.createDecodingMatrix(col_ids);

        // Initialize the source array
        let source: Shard[] = [];
        for (let i=0; i < k; i++) {
            source[i] = {id: i, data: []};
        }

        // Calculate the source data
        for (let l=0; l < shards[0].data.length; l++) {
             for (let c=0; c <inv.cols; c++){
                let rep = F.e(BigInt(0));
                 for (let r=0; r < inv.rows; r++) {
                    rep = F.add(rep, F.mul(F.e(shards[r].data[l]),inv.getValue(c,r)));
                }
                source[c].data[l] = rep;
            }
        }

        // Convert the source data into a buffer
        let dataBigInt:BigInt[] = [];
        for (let l=0; l < source[0].data.length; l++) {
            for (let s=0; s < k; s++) {
                dataBigInt.push(source[s].data[l]);
            }
        }

        const buffer = await bigIntArrayToBuffer(dataBigInt as bigint[], 31);
        const sizeBuffer = buffer.subarray(0, 8);

        const dataSize = sizeBuffer.readBigUInt64LE();
        return buffer.subarray(8, 8 + Number(dataSize));
    }


    static async GenerateLinearCombination(data: bigint[], k: number, alpha: number) : Promise<Shard> {
        // check that data has k elements
        if (data.length != k) {
            throw new Error('Data must have k elements');
        }

        let curve = await ffjs.getCurveFromName('bn128',true);
        let F = curve.Fr;

        let coef =F.e(BigInt(1));

        let rep = F.e(BigInt(0));
        for (let i=0; i < k; i++)
        {
            rep = F.add(rep, F.mul(F.e(data[i]),coef));
            coef = F.mul(coef,F.e(BigInt(alpha)));
        }

        return {id: alpha, data: [rep]};
    }

    /**
     * Encodes the given data into shards.
     * @param {Buffer} data - The data to encode.
     * @param {number} k - The number of data shards.
     * @param {number} n - The total number of shards.
     * @returns {Promise<Shard[]>} The encoded shards.
     */
    static async encode(data:Buffer, k: number, n: number): Promise<Shard[]> {
        // Get the curve
        let curve = await ffjs.getCurveFromName('bn128',true);
        let F = curve.Fr;

        // Write the size of the data into a buffer
        const dataSize = data.length;
        const sizeBuffer = Buffer.alloc(8);
        sizeBuffer.writeBigUInt64LE(BigInt(dataSize));

        // Concatenate the size buffer with the original data
        data = Buffer.concat([sizeBuffer, data]);

        // Convert the byte buffer into an array of bigints
        const dataBigInt = bufferToBigIntArray(data,31);

        // Pad the data with zeros to make it a multiple of k
        while (dataBigInt.length % k != 0) {
            dataBigInt.push(BigInt(0));
        }

        // Hash the data
        let hash = await Poseidon.hash(dataBigInt as bigint[]);
        console.log('hash',hash);

        // Create the shards
        const shards: Shard[] = [];
        for (let i = 0; i < k; i++) {
            shards[i] = {id: i, data: []};
        }

        // Split the data into shards
        for (let i = 0; i < dataBigInt.length; i++) {
            shards[i % k].data.push(dataBigInt[i]);
        }

        // Initialize the reps array
        let reps: Shard[] = [];
        for (let i = 0; i < n; i++) {
            reps[i] = {id: i, data: []};
        }

        // Create the Vandermonde matrix
        let m = await Matrix.createVandermonde(k,n);

        // Calculate the rep values
        for (let l=0; l < shards[0].data.length; l++) {
            for (let c=0; c <m.cols; c++){
                let rep = F.e(BigInt(0));
                for (let r=0; r < m.rows; r++)
                {
                    rep = F.add(rep, F.mul(F.e(shards[r].data[l]),m.getValue(c,r)));
                }
                reps[c].data[l] = rep;
            }
        }

        return reps;
    }
}
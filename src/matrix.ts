import ffjs from "ffjavascript";

/**
 * Class representing a Matrix.
 */
export class Matrix {
    private _rows: number;
    private _cols: number;
    private data: bigint[][];
    private F: any;

    /**
     * Create a Matrix object.
     * @param {number} rows - The number of rows in the matrix.
     * @param {number} cols - The number of columns in the matrix.
     * @param {any} F - The field over which the matrix is defined.
     */
    private constructor(rows: number, cols: number, F: any) {
        this._rows = rows;
        this._cols = cols;
        this.data = [];
        this.F = F;
        for (let i = 0; i < this.cols; i++) {
            this.data[i] = [];
        }
    }

    /**
     * Get the number of rows in the matrix.
     * @returns {number} The number of rows in the matrix.
     */
    get rows(): number {
        return this._rows;
    }

    /**
     * Get the number of columns in the matrix.
     * @returns {number} The number of columns in the matrix.
     */
    get cols(): number {
        return this._cols;
    }

    /**
     * Convert the matrix to a string.
     * @returns {string} A string representation of the matrix.
     */
    toString(): string {
        let res = '';
        for (let i = 0; i < this.rows; i++) {
            for (let j=0; j < this.cols; j++) {
                res += this.F.toString(this.data[j][i]);
                res += ' ';
            }
            res += '\n';
        }
        return res;
    }

    /**
     * Generate the Vandermonde coefficients for a given column.
     * @param {number} id - The column index (as alpha-1).
     * @param {number} k - The number of coefficients to generate.
     * @param {any} F - The field over which the coefficients are defined.
     * @returns {bigint[]} An array of Vandermonde coefficients.
     */
    private static generateVandermondeCoeficients(id: number, k: number, F: any) : bigint[] {
        let coef = F.e(BigInt(1));
        let coeficients = [];
        for (let i=0; i < k; i++) {
            coeficients.push(coef);
            coef = F.mul(coef, F.e(BigInt(id+1)));
        }
        return coeficients;
    }

    /**
     * Create a Vandermonde matrix.
     * @param {number} rows - The number of rows in the matrix.
     * @param {number} cols - The number of columns in the matrix.
     * @returns {Promise<Matrix>} A promise that resolves to a Vandermonde matrix.
     */
    static async createVandermonde(rows: number, cols: number) {
        let curve = await ffjs.getCurveFromName('bn128',true);
        let F = curve.Fr;
        let m = new Matrix(rows, cols, F);
        for (let i = 0; i < m.cols; i++) {
            m.data[i] = Matrix.generateVandermondeCoeficients(i, rows, F);
        }
        return m;
    }

    /**
     * Create a decoding matrix.
     * @param {number[]} col_ids - The column indices to include in the decoding matrix.
     * @returns {Promise<Matrix>} A promise that resolves to the decoding matrix.
     *
     * @remarks This method creates a submatrix from a Vandermonde matrix and inverts it.
     */
    static async createDecodingMatrix(col_ids: number[]) : Promise<Matrix> {

        let curve = await ffjs.getCurveFromName('bn128',true);
        let F = curve.Fr;
        const k= col_ids.length;
        let m = new Matrix(k,k, F);
        for (let i = 0; i < m.cols; i++) {
            m.data[i] = Matrix.generateVandermondeCoeficients(col_ids[i], k, F);
        }

        return m.invert();
    }

    /**
     * Create an identity matrix.
     * @param {number} n - The size of the matrix.
     * @param {any} F - The field over which the matrix is defined.
     * @returns {Matrix} The identity matrix.
     */
    static createIdentity(n: number, F: any): Matrix {
        let m = new Matrix(n, n, F);
        for (let i = 0; i < m.cols; i++) {
            m.data[i] = [];
            for (let j = 0; j < m.rows; j++) {
                if (i==j) {
                    m.data[i][j] = F.e(BigInt(1));
                } else {
                    m.data[i][j] = F.e(BigInt(0));
                }
            }
        }
        return m;
    }

    /**
     * Divide a row of the matrix by a value.
     * @param {number} row - The index of the row to divide.
     * @param {bigint} v - The value to divide by.
     */
    divide_row_by(row: number, v: bigint) {
        for (let j = 0; j < this.cols; j++) {
            let a = this.F.div(this.getValue(row, j), v);
            this.setValue(row, j, a);
        }
    }

    /**
     * Multiply a row of the matrix by a value and subtract the result from another row.
     * @param {number} src - The index of the row to multiply.
     * @param {bigint} v - The value to multiply by.
     * @param {number} dest - The index of the row to subtract from.
     */
    multiply_row_by_and_sub_to_row(src: number, v: bigint, dest: number) {
        for (let j = 0; j < this.cols; j++) {
            let a = this.F.sub( this.getValue(dest, j), this.F.mul(v, this.getValue(src, j)));
            this.setValue(dest, j, a);
        }
    }

    /**
     * Get a value from the matrix.
     * @param {number} col - The column index of the value.
     * @param {number} row - The row index of the value.
     * @returns {bigint} The value at the given indices.
     */
    getValue(col: number, row: number): bigint {
        if (row < 0 || row >= this.rows || col < 0 || col >= this.cols) {
            throw new Error('Index out of bounds');
        }
        return this.data[col][row];
    }

    /**
     * Set a value in the matrix.
     * @param {number} col - The column index of the value.
     * @param {number} row - The row index of the value.
     * @param {bigint} value - The value to set.
     */
    setValue(col: number, row: number, value: bigint): void {
        if (row < 0 || row >= this.rows || col < 0 || col >= this.cols) {
            throw new Error('Index out of bounds');
        }
        this.data[col][row] = value;
    }

    /**
     * Create a copy of the matrix.
     * @returns {Matrix} A copy of the matrix.
     */
    copy() : Matrix {
        let m = new Matrix(this.rows, this.cols, this.F);
        for (let i = 0; i < this.cols; i++) {
            m.data[i] = this.data[i].slice();
        }
        return m;
    }

    /**
     * Invert the matrix.
     * @returns {Matrix} The inverse of the matrix as a new matrix.
     *
     * @remarks This method uses the Gauss-Jordan elimination algorithm to invert the matrix.
     * @remarks The matrix must be square in order to be invertible.
     * @remarks The matrix will be modified and it returns another matrix.
     */
    invert(): Matrix {
        if (this.rows !== this.cols) {
            throw new Error('Cannot invert a non-square matrix');
        }
        let inv = Matrix.createIdentity(this.rows, this.F);

        for (let i = 0; i < this.cols; i++) {
            let pivot = this.getValue(i, i);
            if (pivot === BigInt(0)) {
                throw new Error("non invertible");
            }

            this.divide_row_by(i, pivot);
            inv.divide_row_by(i, pivot); // Corrected this line

            // Eliminate non-zero entries in column i of m and inv
            for (let j = 0; j < this.rows; j++) {
                if (j !== i) {
                    let factor = this.getValue(j, i);
                    this.multiply_row_by_and_sub_to_row(i, factor, j);
                    inv.multiply_row_by_and_sub_to_row(i, factor, j);
                }
            }
        }
        return inv;
    }
}
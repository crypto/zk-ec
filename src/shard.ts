import {bigIntArrayToBuffer, bufferToBigIntArray} from "./buffer";

/**
 * Type definition for a Shard object.
 */
export type Shard = {
    id: number, // The ID of the shard.
    data:bigint[] // The data contained in the shard, represented as an array of bigints.
};

/**
 * Serializes a Shard object to a Buffer.
 * @param shard - The Shard object to be serialized.
 * @returns A promise that resolves to a Buffer representation of the Shard object.
 */
export async function serializeShard(shard: Shard): Promise<Buffer> {
    // Allocate a buffer for the shard ID and write the ID to it.
    const idBuffer = Buffer.alloc(8);
    idBuffer.writeUInt32LE(shard.id);

    // Convert the shard data to a buffer.
    const data = await bigIntArrayToBuffer(shard.data, 32);

    // Concatenate the ID buffer and the data buffer and return the result.
    return Buffer.concat([idBuffer, data]);
}

/**
 * Deserializes a Buffer to a Shard object.
 * @param buffer - The Buffer to be deserialized.
 * @returns A Shard object representation of the Buffer.
 */
export function deserializeShard(buffer: Buffer): Shard {
    // Read the shard ID from the buffer.
    const id = buffer.readUInt32LE();

    // Extract the data from the buffer.
    const data = buffer.subarray(8);

    // Convert the data to an array of bigints.
    const dataBigInt = bufferToBigIntArray(data, 32);

    // Return the deserialized Shard object.
    return {id, data: dataBigInt};
}
import {SnarkProof, Groth16Verify} from "./proof";
import {Shard} from "./shard";
import ffjs from "ffjavascript";
import path from "path";

/**
 * Class representing an Erasure Code Verifier.
 */
export default class ErasureCodeVerifier {
    private _verifierKey: string;

    /**
     * Create an ErasureCodeVerifier.
     * @param {number} k - The number of data shards.
     * @param {number} chunkSize - The number of chunks in a shard.
     */
    constructor(k: number, chunkSize: number) {
        this._verifierKey = path.join(__dirname,`../zk/verifiers/erasure_code_${k}_${chunkSize}.vkey.json`);
    }

    /**
     * Create an ErasureCodeVerifier from a given verifier key.
     * @param {string} verifierKey - The verifier key (as a pathString).
     * @return {ErasureCodeVerifier} The ErasureCodeVerifier instance.
     */
    static fromVerifierKey(verifierKey: string): ErasureCodeVerifier {
        return Object.assign(new ErasureCodeVerifier(0, 0), {_verifierKey: verifierKey});
    }

    /**
     * Get a shard from a proof without verifying the proof.
     * @param {SnarkProof} proof - The proof.
     * @return {Promise<Shard>} The shard.
     */
    async getShardFromProof(proof: SnarkProof) : Promise<Shard> {
        return this.getShard(proof);
    }

    /**
     * Get a shard from a verified proof.
     * @param {SnarkProof} proof - The proof.
     * @return {Promise<Shard | null>} The shard, or null if the proof is not valid.
     */
    async getShardFromVerifiedProof(proof: SnarkProof) : Promise<Shard | null> {
        if (!await Groth16Verify(this._verifierKey, proof)) {
            return null;
        }
        return this.getShard(proof);
    }

    /**
     * Verify a proof.
     * @param {SnarkProof} proof - The proof.
     * @return {Promise<boolean>} True if the proof is valid, false otherwise.
     */
    async verify(proof: SnarkProof) : Promise<boolean> {
        return await Groth16Verify(this._verifierKey, proof);
    }

    /**
     * Build a shard from a proof.
     * @param {SnarkProof} proof - The proof.
     * @return {Promise<Shard>} The shard.
     */
    private async getShard(proof: SnarkProof) : Promise<Shard> {
        const curve = await ffjs.getCurveFromName('bn128',true);
        const F = curve.Fr;
        const alpha = proof.public_signals[proof.public_signals.length-1];
        const id= Number(alpha)- 1;
        const a = proof.public_signals.slice(0,proof.public_signals.length-2).map(x => F.e(x));
        return {id: id, data: a};
    }
}
import path from "path";
import {SnarkProof, Groth16Verify} from "./proof";

export default class LinearCombinationVerifier {
    private _verifierKey: string;

    constructor(k: number) {
        this._verifierKey = path.join(__dirname,`../zk/verifiers/linear_combination_${k}.vkey.json`);
    }

    async verify(proof: SnarkProof): Promise<boolean> {
        return Groth16Verify(this._verifierKey, proof);
    }
}
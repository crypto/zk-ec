import fs from "fs";
import {ErasureCode} from "../src/erasure_code";
import {serializeShard} from "../src/shard";

/**
 * Main function:  encode a file into shards using erasure coding.
 * The file is read, encoded into shards, and each shard is serialized and written to disk.
 */
async function main() {
    // Path to the file to be encoded
    const filePath = 'assets/cat.jpg';
    // Number of data shards
    const k = 3;
    // Total number of shards
    const n = 6;

    // Read the file
    const file = fs.readFileSync(filePath);

    // Encode the file into shards using erasure coding
    let shards = await ErasureCode.encode(file, k, n);

    // Save the encoded shards to disk
    for (let i = 0; i < shards.length; i++) {
        // Get the current shard
        const shard = shards[i];
        // Serialize the shard
        const serializedShard = await serializeShard(shard);
        // Write the serialized shard to disk
        fs.writeFileSync(`assets/cat_${i}.bin`, serializedShard);
    }
}

// Execute the main function and exit the process when it's done
main().then(() => process.exit());
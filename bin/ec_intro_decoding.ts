import fs from "fs";
import {ErasureCode} from "../src/erasure_code";
import {deserializeShard, Shard} from "../src/shard";

/**
 * Function to randomly choose k shards from the given array of shards.
 * @param {Shard[]} shards - The array of shards to choose from.
 * @param {number} k - The number of shards to choose.
 * @returns {Shard[]} An array of k randomly chosen shards.
 */
function randomlyChooseShards(shards: Shard[], k: number): Shard[] {
    let buffered_proofs = shards.map(p => p);
    buffered_proofs.sort(() => Math.random() - 0.5);
    return buffered_proofs.slice(0, k);
}

/**
 * Main function: decode a file from shards using erasure coding.
 * The shards are read, randomly chosen, and then decoded into the original file.
 */
async function main() {
    const k = 3;
    const n = 6;
    let shards: Shard[] = [];

    // Read the n Shards
    for (let i = 0; i < n; i++) {
        const shard = fs.readFileSync(`assets/cat_${i}.bin`);
        shards.push(deserializeShard(shard));
    }

    // Randomly choose k Shards
    const picked_shards = randomlyChooseShards(shards, k);

    picked_shards.forEach((shard, index) => {
        console.log(`Shard ${index}: ${shard.id}`)
    });

    // Decode the file
    const decoded = await ErasureCode.decode(picked_shards, k, n);
    console.log("Decoding successful! (" + decoded.length + " bytes)");
    fs.writeFileSync('assets/cat_dec.jpg', decoded);
}

// Execute the main function and exit the process when it's done
main().then(() => process.exit());
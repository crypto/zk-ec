:bulb: **Prérequis**

Pour initialiser le projet, exécutez les commandes suivantes :

```bash
npm install
```

Pour compiler vos circuits ou votre code, exécutez les commandes suivantes :

```bash
npm run build
```

> :warning: Toujours exécuter ces commandes á la racine du projet.

# Le stockage distribué fiable et prouvé

## Objectif

L'objectif de ce projet est de mettre en place un système de stockage distribué fiable et prouvé.
Pour cela, il faudra écrire des circuits qui permettent de générer et prouver des blocs d'information codée, appelés _shards_, ainsi que de les vérifier.
L'avantage d'une telle solution est de pouvoir vérifier la validité d'une partie de la donnée initiale sans avoir besoin de la totalité de la donnée.


## Introduction

Le stockage distribué est une méthode de stockage de données qui consiste à répartir les données sur plusieurs serveurs.
Une donnée est découpée en plusieurs morceaux, appelés _shards_, et chaque _shard_ est stocké sur un serveur différent.
Cette méthode permet de garantir la disponibilité des données même si un ou plusieurs serveurs tombent en panne.
Pour tolérer les pannes, une solution consiste a répliquer tous les _shards_ sur plusieurs serveurs.
Cependant, cette solution est couteuse en terme de stockage.

Afin d'améliorer la fiabilité du stockage distribué, des codes correcteurs d'effacements sont utilisés.
Ces codes permettent de reconstruire les données perdues en cas de panne de serveur.

### Les codes a effacement

Les codes correcteurs d’effacements sont des codes correcteurs d’erreurs utilisés sur un canal de communication où les erreurs sont des effacements, c’est-à-dire des pertes de symboles.

Soit un message $m = (m_0, m_1, \ldots, m_{k-1})$ constitué de $k$ symboles de $F_q$. Soit G une matrice de taille $k \times n$, avec n > k, et dont les lignes sont linéairement indépendantes.
L'opération d'encodage consiste à multiplier le message par la matrice G pour obtenir un message codé $c = mG$. Le code utilisé est de paramètre $(n, k)$.

Afin d'obtenir un code MDS (Maximum Distance Separable), c'est a dire un code qui peut corriger n'importe quel effacement de taille $n-k$, on peut utiliser une matrice de *[Vandermonde](https://fr.wikipedia.org/wiki/Matrice_de_Vandermonde)*.

### Le stockage distribué des _shards_ codés

Grace à ces codes, il est possible de stocker de manière distribuée les _shards_ codés sur plusieurs serveurs.
Ainsi, si un serveur tombe en panne, il est possible de reconstruire les données perdues à partir des données stockées sur les autres serveurs.

Face a un système de stockage sans redondance, c'est a dire en dupliquant les données sur plusieurs serveurs, le stockage distribué permet de stocker les données de manière plus efficace.

Par exemple, dans un systeme distribué composé de 10 noeuds, en tolérant 2 pannes parmi les noeuds, sans code a effacement, il faudrait stocker 3 fois la donnée pour garantir la disponibilité.
Avec un code a effacement de paramètre (10, 8), chaque noeud stocke seulement 1/8eme de la donnée pour la meme disponibilité, soit 20 % de redondance.

### Controler la validité des données

Lorsque les données sont codées et reparties, il est impossible, pour un noeud, de déterminer si les données qu'il stocke sont valides.
Pour remédier à ce problème, nous allons prouver que le _shard_ qu'il stocke est bien une partie codée de la donnée originale.

## Les codes a effacement en pratique

Dans le dossier `bin`, vous trouverez les fichiers `ec_intro_encoding.ts` et `ec_intro_decoding.ts`.

`ec_intro_encoding.ts` permet de générer des _shards_ codés a partir d'un fichier source. Le script lit le fichier `assets/cat.jpg`, le découpe en _shards_ de taille 176 octets, et les encode avec un code a effacement de paramètres (6,3).

`ec_intro_decoding.ts` permet de décoder le fichier original a partir des _shards_ codés. Le script lit les _shards_ codés stockés dans le dossier `assets`, les décode, et stocke le fichier décodé dans le dossier `assets` avec le nom `cat_decoded.jpg`.

Familiarisez vous avec ces scripts, et executez les pour comprendre le fonctionnement des codes a effacement.

Vous pouvez exécuter ces scripts en utilisant la commande suivante :

```bash
npm run ec_intro_encoding
npm run ec_intro_decoding
```


## Prouver un code a effacement

Pour prouver la validité des _shards_, nous allons créer un circuit qui permet de générer un _shard_ codé et la preuve associée.

- Dans un premier temps, nous allons créer un circuit qui permet de générer une combinaison linéaire de k signaux de données.
- Dans un second temps, nous allons créer un circuit qui permet de générer un code a effacement de paramètre $(n, k)$.
- A ce circuit, nous allons ajouter un signal de sortie qui est le hash du tableau de données d'entrée.

### Composants Circom de base

#### `LinearCombination`

- Dans le dossier `circuits/components`, créez le fichier `linear_combination.circom`.
- Ecrivez le template `LinearCombination` qui prend en entrée un tableau de `k` signaux de données, un coefficient `alpha`, et en sortie un signal étant la combinaison linéaire de ces données.
- Ce composant sera paramétré par le nombre de signaux de données k.

Il devra prouver par un ensemble de contraintes que la sortie est bien la combinaison linéaire des données d'entrée telle que:

$$ \text{output} = \sum_{i=0}^{k-1} \alpha^i \times \text{input}_i  $$

Dans le dossier `circuits`, écrivez le fichier `linear_combination_3.circom` et instanciez le template `LinearCombination` avec k=3 comme étant le composant `main`.

Verifiez la validation de votre circuit en executant les tests associés : `npm run test:linear_combination`

#### `ErasureCode`

- Dans le dossier `circuits/components`, créez le fichier `erasure_code.circom`.
- Ecrivez le template `ErasureCode` qui prend en entrée un tableau de `sz * k` signaux de données, et un coefficient `alpha`, et en sortie un tableau de `sz` signaux représentant les `sz` combinaisons lineaires ainsi qu'un signal étant le hash du tableau de données.
- Ce composant sera paramétré par le nombre de signaux de données k et la taille des signaux de données sz.
- Ce composant devra utiliser le composant `LinearCombination` et `HashTree`.
    - D'abord, il devra hasher les données d'entrée.
    - Ensuite, il devra générer les combinaisons lineaires des données d'entrée.


### Instances du circuit

- Dans le dossier `circuits`, écrivez le fichier `erasure_code_3_1.circom` et instanciez le template `ErasureCode` avec `k=3` et `sz=1` comme étant le composant `main`.
- Faites de même avec les paramètres `k=4` et `sz=[2,6,8]` dans les fichiers `erasure_code_<k>_<sz>.circom`.
- Instanciez a chaque fois le template `ErasureCode` avec les paramètres correspondants.

Vérifiez la validation de vos circuits en executant les tests associés : `npm run test:erasure_code`

# Prouver des _shards_

Nous allons maintenant utiliser les circuits que nous avons créés pour prouver la validité des _shards_.
Pour cela, nous allons générer des _shards_ codés de l'image `assets/cat.jpg` avec un code a effacement de paramètre (6,3).

## Instance du circuit `ErasureCodes`

- Dans le dossier `circuits`, créez une instance du circuit `ErasureCode` paramétré avec $k=3$ et $sz=176$.
- Ces paramètres permettent de générer des _shards_ contenant chacun 176 élements de $F_q$, soit $5456$ octets, ce qui est suffisant pour stocker un _shard_ de l'image `assets/cat.jpg`.

## Prouver des _shards_

- Dans le dossier `bin`, créez le fichier `ec_prover.ts`.
- Utiliser les classes mises a disposition pour générer des _shards_ prouvés et les stocker sur le disque en les serialisant :
    - Lire le fichier `assets/cat.jpg` dans un buffer.
    - En utilisant la fonction `ErasureProver.proveShards`, générez les $n$ _shards_ codés et prouvés.
    - Pour chacun des _shards_, serialisez le _shard_ et la preuve associée a l'aide de la fonction `serializeSnarkProof`.
    - Stockez ces données sérialisés sur le disque dans le dossier `assets`.

Pour tester votre code, vous pouvez exécuter le script `ec_prover.ts` avec la commande suivante :

```bash
npm run ec_prover
```

## Verification de vos _shards_

- Dans le dossier `bin`, créez le fichier `ec_verifier.ts`.
- Utiliser les classes mises a disposition pour vérifier les _shards_ stockés sur le disque :
    - Lire les _shards_ et leurs preuves associées sérialisés stockés dans le dossier `assets`.
    - Pour chacun des _shards_, désérialisez le _shard_ et la preuve associée a l'aide de la fonction `deserializeSnarkProof`.
    - Utilisez la fonction `ErasureVerifier.getShardFromVerifiedProof` pour vérifier la validité du _shard_.
- Décodez le fichier original a partir de k _shards_ valides a l'aide de la fonction `ErasureCode.decode`.
- Stockez le fichier décodé sur le disque dans le dossier `assets` avec le nom `cat_decoded.jpg`.
- Vérifiez que le fichier décodé est bien identique au fichier original.

Pour tester votre code, vous pouvez exécuter le script `ec_verifier.ts` avec la commande suivante :

```bash
npm run ec_verifier
```

# Detecter la corruption des _shards_

Dans le dossiers `assets/shards`, vous trouverez des _shards_ stockés de maniere serialisée.
Il s'agit shards résultant de l'encodage d'une image au format jpg, utilisant un circuit circom et le code de paramètres (40,20). Ces _shards_ sont stockés avec leurs preuves associées.

Utilisez les classes mises a disposition pour décoder l'image. Pour cela, dans le dossier `bin`, créez le fichier `ec_corrupted_verifier.ts`.

Attention, certains shards sont corrompus, d'autres non. Vous devez trouver $k=20$ shards non corrompus pour pouvoir décoder l'image.

Ces _shards_ et leurs preuves ont été générés a partir d'un circuit dont la clé de verification `vkey.json` vous est donnée, et est stockée dans le dossier `assets/shards`.
Vous n'avez pas besoin de la clé de preuve pour vérifier les _shards_, ni du circuit.

Vous avez le droit de faire du _bruteforce_ pour trouver une combinaison de $k$ shards valides parmi les $n=40$ mis a disposition.

Si vous souhaitez éviter la solution _bruteforce_, qui consiste a tester les $C_n^k$ combinaisons possibles, vous pouvez utiliser la fonction `ErasureVerifier.verify` pour vérifier la validité d'un _shard_.

Vous pouvez exécuter le script `ec_corrupted_verifier.ts` avec la commande suivante :

```bash
npm run ec_corrupted_verifier
```